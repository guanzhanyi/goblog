## 技能清单
1. gin框架
2. zap日志库
3. Viper配置管理
4. swagger生成文档
5. JWT认证
6. 令牌桶限流
7. Go语言操作MySQL
8. Go语言操作Redis

## go-web脚手架
* conf        配置 viper config.yaml
* controller  路由 参数校验 请求转发 auth.go request.go response.go user.go
* dao         数据库 mysql redis
* logger      日志 zap logger.go
* logic       业务逻辑
* models      
* pkg         中间件
* routers     路由
* settings    设置